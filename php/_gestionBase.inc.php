<?php

session_start();
if (isset($_REQUEST["logout"])) {
    session_unset();
}

if (isset($_REQUEST["adrMel"]) && isset($_REQUEST["mdp"])) {
    $resultat = connexion($_REQUEST["adrMel"], $_REQUEST["mdp"]);
    if ($resultat == true) {
        $_SESSION["adrMel"] = $resultat["adrMel"];
        $_SESSION["mdp"] = $resultat["mdp"];
        $_SESSION["codeUser"] = $resultat["code"];
    }
}

if ($_SERVER['PHP_SELF'] != "/projects/tholdi/html/acceuilT.php") {
    if (!(isset($_SESSION["adrMel"]) && isset($_SESSION["mdp"]))) {
        header("Location: ../html/acceuilT.php");
    }
}

/* Fonction pour se connecter à la base de données */
function gestionnaireDeConnexion() {
    $pdo = null;
    try {
        $pdo = new PDO(
                'mysql:host=localhost;dbname=tholdi', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
        );
    } catch (PDOException $err) {
        $messageErreur = $err->getMessage();
        error_log($messageErreur, 0);
    }
    return $pdo;
}

/* Fonction pour reccuperer les pays */
function obtenirPays() {
    $detailPays = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * from Pays ";
        $resultat = $pdo->query($req);
        $detailPays = $resultat->fetchAll();
    }
    return $detailPays;
}

/*Fonction qui affiche les données de réservation*/
function obtenirReservation(){
    $detailReservation = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select R.dateDebutReservation, R.dateFinReservation from reservation R where R.codeReservation =". $_SESSION["codeReservation"];
        $resultat = $pdo->query($req);
        $detailReservation = $resultat->fetchAll();
    }
    return $detailReservation;
}

/*Fontion qui permet d'obtenir les villes de mise à disposition de la reservation*/
function obtenirVilleMiseDispo(){
    $detailVilleMiseDispo = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select R.codeVilleMiseDispo, V.nomVille from reservation R, ville V where R.codeReservation =" . $_SESSION["codeReservation"]." and R.codeVilleMiseDispo = V.codeVille";
        $resultat = $pdo->query($req);
        $detailVilleMiseDispo = $resultat->fetchAll();
    }
    return $detailVilleMiseDispo;
}

/*Fontion qui permet d'obtenir les villes de mise à disposition de la reservation*/
function obtenirVilleRendre(){
    $detailVilleRndre = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select R.codeVilleRendre, V.nomVille from reservation R, ville V where R.codeReservation =" . $_SESSION["codeReservation"]." and R.codeVilleRendre = V.codeVille";
        $resultat = $pdo->query($req);
        $detailVilleRndre = $resultat->fetchAll();
    }
    return $detailVilleRndre;
}

/* Fonction pour la connexion */
function connexion($adrMel, $mdp) {
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $sql = "Select * From personne Where adrMel =:adrMel And mdp =:mdp";
        $prep = $pdo->prepare($sql);
        $prep->bindParam(':adrMel', $adrMel, PDO::PARAM_STR);
        $prep->bindParam(':mdp', $mdp, PDO::PARAM_STR);
        $prep->execute();
        $resultat = $prep->fetch();
        if (is_array($resultat) && count($resultat) >=1) {
            return $resultat;
        }
    }
    return false;
}

/* Fonction pour reccuperer les villes */
function obtenirVille() {
    $detailVille = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * from Ville ";
        $resultat = $pdo->query($req);
        $detailVille = $resultat->fetchAll();
    }
    return $detailVille;
}

/* Fonction pour reccuperer les type de conteneur */
function obtenirConteneur() {
    $detailTypeConteneur = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * from Typecontainer ";
        $resultat = $pdo->query($req);
        $detailTypeConteneur = $resultat->fetchAll();
    }
    return $detailTypeConteneur;
}

/* Fonction pour reccuperer les attributs des types de container */
function obtenirAttribut() {
    $detailAttribut = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * from Typecontainer ";
        $resultat = $pdo->query($req);
        $detailAttribut = $resultat->fetchAll();
    }
    return $detailAttribut;
}

/*Fonction pour récuperer les données des personnes inscrites*/
function obtenirPersonne() {
    $detailPersonne = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * from personne ";
        $resultat = $pdo->query($req);
        $detailPersonne = $resultat->fetchAll();
    }
    return $detailPersonne;
}

/* Fonction pour l'inscription */
function inscription($code, $raisonSociale, $adresse, $cp, $ville, $adrMel, $telephone, $contact, $codePays, $mdp) {
    $reussi = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $code = $pdo->quote($code);
        $raisonSociale = $pdo->quote($raisonSociale);
        $adresse = $pdo->quote($adresse);
        $cp = $pdo->quote($cp);
        $ville = $pdo->quote($ville);
        $adrMel = $pdo->quote($adrMel);
        $telephone = $pdo->quote($telephone);
        $contact = $pdo->quote($contact);
        $codePays = $pdo->quote($codePays);
        $mdp = $pdo->quote($mdp);

        $req = "insert into personne values($code, $raisonSociale, $adresse, $cp, $ville, $adrMel, $telephone, $contact, $codePays, $mdp)";
        $resultat = $pdo->exec($req);
        if ($resultat == 1) {
            $reussi = true;
        }
    }
    return $reussi;
}

/* Fonctions des date et des lieux de reservation */
function reservation($dateDebutReservation, $dateFinReservation, $volumeEstime, $codeVilleMiseDispo , $codeVilleRendre, $code){
    $succes = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        
        $dateDebutReservation = $pdo->quote($dateDebutReservation);
        $dateFinReservation = $pdo->quote($dateFinReservation);
        $volumeEstime = $pdo->quote($volumeEstime);
        $codeVilleMiseDispo = $pdo->quote($codeVilleMiseDispo);
        $codeVilleRendre = $pdo->quote($codeVilleRendre);
        $code = $pdo->quote($code);
        
        $req = "insert into RESERVATION (dateDebutReservation, dateFinReservation, dateReservation, volumeEstime, codeVilleMiseDispo, codeVilleRendre, code) values ($dateDebutReservation, $dateFinReservation, curdate(), $volumeEstime, $codeVilleMiseDispo , $codeVilleRendre, $code)";    
        $resultat = $pdo->exec($req);
        $_SESSION["codeReservation"] = $pdo->lastInsertId();
        if ($resultat == 1) {
            $succes = true;
        }
        //retourner le code de la réservation crée Cf last_insertId() fcontion pdo
    }
    return $_SESSION["codeReservation"];
}

/*Fonction qui permet de reserver les type de conteneuurs*/
function reserver($codeReservation, $typeContainer, $qteReserver) {
    $reussi = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $codeReservation = $pdo->quote($codeReservation);
        $typeContainer = $pdo->quote($typeContainer);
        $qteReserver = $pdo->quote($qteReserver);

        $req = "insert into reserver values($codeReservation, $typeContainer, $qteReserver)";
        $resultat = $pdo->exec($req);
        if ($resultat == 1) {
            $reussi = true;
        }
    }
}

/* Fonction de récupération de code de reservation */
function recuperationCodeReservation(){
    $pdo = gestionnaireDeConnexion();
    $resultat = $_SESSION["codeReservation"];
    return $resultat;
}

/*Affichage des conteneurs de la réservation*/
function afficherConteneur(){
    $resultat = false;
    $pdo = gestionnaireDeConnexion();
    if($pdo != false){
        $req = "Select T.*, R.qteReserver from typeContainer T, reserver R where R.codeReservation = ". $_SESSION["codeReservation"]." and R.typeContainer=T.typeContainer";
        $resultat = $pdo->query($req);
    }
    return $resultat;
}


/*Fonction fonction pour obtenir le dernier client*/
function afficherClient(){
    $resultat = false;
    $pdo = gestionnaireDeConnexion();
    if($pdo != false){
        $req = "Select * from personne where code =  (Select Max(code) from personne)";
        $resultat = $pdo->query($req);
    }
    return $resultat;
}

/*Fonction pour creer un numero de devis*/
function numeroDevis($codeDevis){
    $reussi = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $codeDevis = $pdo->quote($codeDevis);
        $req = "update  reservation set codeDevis=$codeDevis where codeReservation=". $_SESSION["codeReservation"];
        $resultat = $pdo->exec($req);
        if ($resultat == 1) {
            $reussi = true;
        }
    }
    return $reussi;
}

/*Fonction pour creer un devis*/
function devis(){
    $reussi = false;
    $pdo = gestionnaireDeConnexion();
    if($pdo != false){
        $montantDevis = "Select (R.qteDevis * T.prix) as montantDevis from Reserver R, TypeContainer T Where R.typeContainer = T.TypeContainer and codeReservation =" . $_SESSION["codeReservation"];
        $nbContainer = "Select count (*) as nbContainer from Reserver where codeReservation =" . $_SESSION["codeReservation"];
        $volume = "Select volumeEstime from reservation where codeReservation = ". $_SESSION["codeReservation"];
        
        $req = "insert into DEVIS ( montantDevis, volume, nbContainer) values ($montantDevis,$volume, $nbContainer )";
        $resultat = $pdo->exec($req);
        $_SESSION["codeDevis"] = $pdo->lastInsertId();
        if ($resultat == 1) {
            $reussi = true;
        }
    }
    return $_SESSION["codeDevis"];
}