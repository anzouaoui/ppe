<?php

require ('../php/_gestionBase.inc.php');
require ('../php/devis.php');
$date=  date('d/m/Y');
$pdf = new PDF_Invoice('P', 'mm', 'A4');
$pdf->AddPage();
$pdf->ajouterPage(1);
$pdf->Image('minilogo.jpg',10,6,30);
$pdf->ajouterSociete("THOLDI", "5 Rue du Pressoir\n78570 CHANTELOUP\nFRANCE");
$pdf->ajouterDevis(01);
$pdf->ajouterDate($date);
$collectionClient = afficherClient();
if ($collectionClient != null) {
    foreach ($collectionClient as $clientCourant) {
        $raisonSociale = $clientCourant["raisonSociale"];
        $pdf->ajouterClient($raisonSociale);

        $adresse = $clientCourant["adresse"];
        $pdf->ajouterAdresseClient($adresse);
    }
}

$pdf->ajouterReferences("devis".$_SESSION["codeReservation"]." du 03/05/2016");
$cols = array("REFERENCE" => 23,
    "DESIGNATION" => 78,
    "QUANTITE" => 22,
    "P.U. HT" => 26,
    "MONTANT H.T." => 30,
    "TVA" => 11);
$pdf->ajouterColones($cols);

$cols = array("REFERENCE" => "L",
    "DESIGNATION" => "L",
    "QUANTITE" => "C",
    "P.U. HT" => "R",
    "MONTANT H.T." => "R",
    "TVA" => "C");
$pdf->ajouterFromat($cols);
$pdf->ajouterFromat($cols);

$y = 109;

$collectionTypeContainer = afficherConteneur();
if ($collectionTypeContainer != null) {
    foreach ($collectionTypeContainer as $conteneurCourant) {
        $line = array(
            "REFERENCE" => $conteneurCourant["typeContainer"],
            "DESIGNATION" => $conteneurCourant["libelleTypeContainer"],
            "QUANTITE" => $conteneurCourant["qteReserver"],
            "P.U. HT" => 150,
            "MONTANT H.T." => 150,
            "TVA" => 20
        );
        $taille = $pdf->ajouterLigne($y, $line);
        $y += $taille + 2;
    }
}

$pdf->ajouterCadreTVA();

$tot_prods = array(
    array("px_unit" => 600, "qte" => 1, "tva" => 1),
    array("px_unit" => 10, "qte" => 1, "tva" => 1)
);

$tab_tva = array(
    "1" => 19.6,
    "2" => 5.5
);

$params = array(
    "RemiseGlobale" => 1,
    "remise_tva" => 1,
    "remise" => 0,
    "remise_percent" => 10,
    "FraisPort" => 1,
    "portTTC" => 10,
    "portHT" => 0,
    "portTVA" => 19.6,
    "AccompteExige" => 1,
    "accompte" => 0,
    "accompte_percent" => 15,
    "Remarque" => "Avec un acompte, svp...");

$pdf->ajouterTVAs($params, $tab_tva, $tot_prods);
$pdf->ajouterCadreEuros();
$pdf->Output();
