<!DOCTYPE html>
<?php include '../html/entete.php'; ?>
<html lang="fr">
    <head>
        <link rel="stylesheet" href="../css/conteneursT.css">
    </head>
    <section class="main container">
        <div class="miga-de-pan">
            <ol class="breadcrumb">
                <li><a href="../html/acceuilT.php">Page d'acceuil</a></li>
                <li><a href="../html/aProposT.php">&Agrave; propos</a></li>
                <li><a href="../html/conteneursT.php">Conteneurs</a></li>
            </ol>
        </div>

        <div class="row">
            <section class="posts col-md-9">
                <div class="miga-de-pan">
                    <ol class="breadcrumb">
                        <li class="active">&Agrave; propos</li>
                    </ol>
                </div>

                <article class="post clearfix">
                    <p class="post-contenido text-justify">
                        La société Tholdi est spécialisée dans la gestion de containeurs destinés au 
                        transport de marchandises, et spécialement aux transports maritimes.<br><br>
                        Notre activité consiste à gérer le déchargement et la réception des containeurs 
                        contrôle de leur provenance et du transporteur maritime), à gérer le placement 
                        en zone de stockage temporaire, et à gérer le chargement des containeurs sur les 
                        remorques de transport routier ou de transport ferroviaire.<br><br>
                        Notre siège social est situé en région parisienne :<br>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10494.897936781937!2d2.3117679!3d48.8825286!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3d8ec5cd1d42a5d0!2sAssociation+pour+le+D%C3%A9veloppement+des+Ports+Fran%C3%A7ais+A.D.P.F!5e0!3m2!1sfr!2sfr!4v1448133644050" width="700" height="300" frameborder="0" style="border:0" allowfullscreen></iframe><br><br>
                        Nos zones d’activités se situe dans les ports de France (Le Havre et Marseille), d’Allemagne (Hambourg), de Belgique (Anvers) et du Pays-Bas (Rotterdam).
                    </p>
                    <div class="col-md-12 col-sm-9 col-xs-9" id="container">
                        <div class="row">
                            <h1>Les ports</h1>
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-custom">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#dry" aria-expanded="true" aria-controls="dry">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                Port du Havre
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="dry" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body animated zoomOut">
                                            <img class="img-thumbnail" src="../image/portHavre.jpg" alt="portHavre" width="304" height="236"/>
                                            <p class="post-conten text-justify">
                                                <br><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d41480.05702613242!2d0.0983136244443462!3d49.474994164160265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e02e5851ad06af%3A0xe4bbb9828a3bd4df!2sPort+du+Havre%2C+76600!5e0!3m2!1sfr!2sfr!4v1448122857050" width="700" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#Reefer" aria-expanded="false" aria-controls="Reefer">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                Port de Marseille
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="Reefer" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body animated zoomOut">
                                            <img class="img-thumbnail" src="../image/portMarseille.jpg" alt="portMarseille" width="404" height="236"/>
                                            <p class="post-conten text-justify">
                                                <br><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23231.273581885514!2d5.347249411899033!3d43.29522321463549!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12c9c0c6a1843729%3A0x7d3f3acf189dc3b1!2sVieux-Port+de+Marseille!5e0!3m2!1sfr!2sfr!4v1448131391884" width="700" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <i class="glyphicon glyphicon-plus"></i>                    
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#OpenTop" aria-expanded="false" aria-controls="OpenTop">                            
                                                Port de Hambourg
                                            </a>                        
                                        </h4>
                                    </div>
                                    <div id="OpenTop" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body animated zoomOut">
                                            <img class="img-thumbnail" src="../image/portHamburg.png" alt="portHamburg" width="304" height="236"/>
                                            <p class="post-conten text-justify">
                                                <br><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d303726.7369502031!2d9.687801000426536!3d53.50660457145582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b184876a599be7%3A0x928d4aacd4e8f95e!2sPort+de+Hambourg%2C+Hambourg%2C+Allemagne!5e0!3m2!1sfr!2sfr!4v1448131466117" width="700" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                        <h4 class="panel-title">
                                            <i class="glyphicon glyphicon-plus"></i>                    
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#FlatRack" aria-expanded="false" aria-controls="FlatRack">                            
                                                Port d'Anvers
                                            </a>                        
                                        </h4>
                                    </div>
                                    <div id="FlatRack" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body animated zoomOut">
                                            <img class="img-thumbnail" src="../image/portAnvers.jpg" alt="portAnvers" width="304" height="236"/>
                                            <p class="post-conten text-justify">
                                                <br><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d159933.8187222319!2d4.217190245658391!3d51.2197035094118!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c40a777b040dcb%3A0x7c945a50439ed3b2!2sPort+d&#39;Anvers%2C+2030+Anvers%2C+Belgique!5e0!3m2!1sfr!2sfr!4v1448131532151" width="700" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <div class="panel-heading" role="tab" id="headingFive">
                                        <h4 class="panel-title">
                                            <i class="glyphicon glyphicon-plus"></i>                    
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#Citerne" aria-expanded="false" aria-controls="Citerne">                            
                                                Port de Rotterdam
                                            </a>                        
                                        </h4>
                                    </div>
                                    <div id="Citerne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body animated zoomOut">
                                            <img class="img-thumbnail" src="../image/portRotterdam.jpg" alt="portRotterdam" width="304" height="236"/>
                                            <p class="post-conten text-justify">
                                                <br><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2461.5981242457!2d4.482368915752098!3d51.90479767970244!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c43364d44f9f39%3A0xc5831d410339551e!2sPort+of+Rotterdam%2C+Wilhelminakade+909%2C+3072+AP+Rotterdam!5e0!3m2!1sfr!2sfr!4v1448131620889" width="700" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                                            </p>
                                        </div>
                                    </div>
                                </div>			
                            </div>
                        </div>
                    </div>
                </article>
            </section>
            <aside class="col-md-3 hidden-xs hidden-sm">
                <h4>Catégorie</h4>
                <div class="list-group">
                    <a href="../html/acceuilT.php" class="list-group-item">Page d'acceuil</a>
                    <a href="../html/aProposT.php" class="list-group-item active">&Agrave; propos</a>
                    <a href="../html/conteneursT.php" class="list-group-item">Conteneurs</a>
                </div>

                <h4>Articles Récents</h4>
                <a href="http://www.atelier.net/trends/articles/fret-maritime-gerer-complexite-grace-software_439129" class="list-group-item">
                    <h4 class="list-group-item-heading">Fret maritime : gérer la complexité grâce au software</h4>
                    <p class="list-group-item-text"></p>
                </a>

                <a href="http://www.lepoint.fr/economie/transport-maritime-cma-cgm-veut-racheter-neptune-orient-lines-07-12-2015-1987771_28.php" class="list-group-item">
                    <h4 class="list-group-item-heading">Transport maritime : CMA CGM veut racheter Neptune Orient Lines</h4>
                    <p class="list-group-item-text"></p>
                </a>

                <a href="http://www.francetvinfo.fr/meteo/climat/cop21/cop21-pourquoi-l-accord-ne-suffira-pas-pour-sauver-la-planete_1221853.html" class="list-group-item">
                    <h4 class="list-group-item-heading">COP21 : pourquoi l'accord ne suffira pas pour sauver la planète</h4>
                    <p class="list-group-item-text"></p>
                </a>

                <a href="http://www.liberation.fr/planete/2015/12/10/les-negociateurs-reculent-face-aux-transports-aeriens-et-maritimes_1419759" class="list-group-item">
                    <h4 class="list-group-item-heading">Les négociateurs reculent face aux transports aériens et maritimes</h4>
                    <p class="list-group-item-text"></p>
                </a>

                <a href="http://meretmarine.com/fr/node/136695" class="list-group-item">
                    <h4 class="list-group-item-heading">CMA CGM : Le plus gros porte-conteneurs accueilli aux USA</h4>
                    <p class="list-group-item-text"></p>
                </a>
            </aside>
        </div>
    </section>
    <?php include_once '../html/piedPage.php'; ?>

    <script src="../java/jquery.js"></script>
    <script src="../bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
    <script src="../java/conteneursT.js"></script>
</body>
</html>