<!DOCTYPE html>
<?php include '../html/entete.php'; ?>
<html lang="fr">
    <head>
        <link rel="stylesheet" href="../css/galleryT.css">
    </head>	
    <div class="container">
        <div id="main_area">
            <!-- Slider -->
            <div class="row">
                <div class="col-sm-6" id="slider-thumbs">
                    <!-- Bottom switcher of slider -->
                    <ul class="hide-bullets">
                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-0">
                                <img src="../image/logo.png">
                            </a>
                        </li>

                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-1"><img src="../image/bateau1.jpg"></a>
                        </li>

                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-2"><img src="../image/bateau2.jpg"></a>
                        </li>

                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-3"><img src="../image/container1.jpg"></a>
                        </li>

                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-4"><img src="../image/container2.jpg"></a>
                        </li>

                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-5"><img src="../image/container3.png"></a>
                        </li>
                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-6"><img src="../image/conteneurs.jpg"></a>
                        </li>

                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-7"><img src="../image/port1.jpg"></a>
                        </li>

                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-8"><img src="../image/port2.jpg"></a>
                        </li>

                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-9"><img src="../image/port3.jpg"></a>
                        </li>
                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-10"><img src="../image/port4.jpg"></a>
                        </li>

                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-11"><img src="../image/route1.jpg"></a>
                        </li>

                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-12"><img src="../image/route2.jpg"></a>
                        </li>

                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-13"><img src="../image/train1.jpg"></a>
                        </li>
                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-14"><img src="../image/train2.jpg"></a>
                        </li>

                        <li class="col-sm-3">
                            <a class="thumbnail" id="carousel-selector-15"><img src="../image/minilogo.gif"></a>
                        </li>
                    </ul>
                </div>

                <div class="col-sm-6">
                    <div class="col-xs-12" id="slider">
                        <!-- Top part of the slider -->
                        <div class="row">
                            <div class="col-sm-12" id="carousel-bounding-box">
                                <div class="carousel slide" id="myCarousel">
                                    <!-- Carousel items -->
                                    <div class="carousel-inner">
                                        <div class="active item" data-slide-number="0">
                                            <img src="../image/logo.png"></div>

                                        <div class="item" data-slide-number="1">
                                            <img src="../image/bateau1.jpg"></div>

                                        <div class="item" data-slide-number="2">
                                            <img src="../image/bateau2.jpg"></div>

                                        <div class="item" data-slide-number="3">
                                            <img src="../image/container1.jpg"></div>

                                        <div class="item" data-slide-number="4">
                                            <img src="../image/container2.jpg"></div>

                                        <div class="item" data-slide-number="5">
                                            <img src="../image/container3.png"></div>

                                        <div class="item" data-slide-number="6">
                                            <img src="../image/conteneurs.jpg"></div>

                                        <div class="item" data-slide-number="7">
                                            <img src="../image/port1.jpg"></div>

                                        <div class="item" data-slide-number="8">
                                            <img src="../image/port2.jpg"></div>

                                        <div class="item" data-slide-number="9">
                                            <img src="../image/port3.jpg"></div>

                                        <div class="item" data-slide-number="10">
                                            <img src="../image/port4.jpg"></div>

                                        <div class="item" data-slide-number="11">
                                            <img src="../image/route1.jpg"></div>

                                        <div class="item" data-slide-number="12">
                                            <img src="../image/route2.jpg"></div>

                                        <div class="item" data-slide-number="13">
                                            <img src="../image/train1.jpg"></div>

                                        <div class="item" data-slide-number="14">
                                            <img src="../image/train2.jpg"></div>

                                        <div class="item" data-slide-number="15">
                                            <img src="../image/minilogo.gif"></div>
                                    </div>

                                    <!-- Carousel nav -->
                                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/Slider-->
            </div>

        </div>
    </div>
<?php include_once '../html/piedPage.php'; ?>
    <script src="../java/jquery.js"></script>
    <script src="../bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
    <script src="../java/galleryT.js"></script>
</body>
</html>