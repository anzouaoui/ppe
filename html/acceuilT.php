<!DOCTYPE html>
<?php include_once '../html/entete.php'; ?>

<section class="main container">
    <div class="miga-de-pan">
        <ol class="breadcrumb">
            <li><a href="../html/acceuilT.php">Page d'acceuil</a></li>
            <li><a href="../html/aProposT.php">&Agrave; propos</a></li>
            <li><a href="../html/conteneurT.php">Conteneurs</a></li>
        </ol>
    </div>

    <div class="row">
        <section class="posts col-md-9">
            <div class="miga-de-pan">
                <ol class="breadcrumb">
                    <li class="active">Acceuil</li>
                </ol>
            </div>

            <article class="post clearfix">
                <a href="../image/logo.PNG" class="thumb pull-left">
                    <img class="img-thumbnail" src="../image/logo.PNG" alt="">
                </a>
                <p class="post-contenido text-justify">
                    En France on compte :<br>
                    &#8226; 72% des importations et exportations <br>
                    &#8226; 15 millions de passagers transportés chaque année <br>
                    &#8226; 214 navires de commerce <br>
                    &#8226; + 100 millions de tonnes de marchandises chaque années <br><br>
                    <iframe class="posts col-md-6" width="520" height="315" src="https://www.youtube.com/embed/-TmEqA7wHQI" frameborder="0" allowfullscreen></iframe><br>
                    &#8594; 90% des marchandises transportées dans le monde <br>
                    &#8594; 25 mille milliards de tonnes par la MER <br>
                    7 mille milliards pour les RAILS <br>
                    3 mille milliards pour les ROUTES <br><br>
                    &#8594; Le transport maritime est respectueux de l'environnement (7 fois moins polluant, consomme 7 fois moins d'énergie que le transport routier).  <br>
                    &#8594; C'est également un secteur qui est créateur d'emploi. En mer comme sur terre, il propose des emplois hautement qualifiés et diversifiés : 10 000 navigants et 6 500 sédentaires. <br>
                    &#8594; Le coût de transport par mer est très concurrentiel, on compte : 
                    0,10 euro le 1kg de café, moins d'un centime par litre d'essence.<br>
                    &#8594; C'est un acteur essentiel de l'économie. Sa grande capacité, sa productivité, ses performances environnementales et la haute technologie des services maritimes 
                    font de ce secteur un secteur d'avenir.<br>
                </p>
            </article>
            <!-- plusieurs pages -->
            <!--<nav>
                    <div class="center-block">
                            <ul class="pagination">
                                    <li class="disabled"><a href="#">&laquo;<span class="sr-only">Anterior</span></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">&raquo; <span class="sr-only">Siguiente</span></a></li>
                            </ul>
                    </div
            </nav>-->
        </section>
        <aside class="col-md-3 hidden-xs hidden-sm">
            <h4>Catégorie</h4>
            <div class="list-group">
                <a href="../html/acceuilT.php" class="list-group-item active">Page d'acceuil</a>
                <a href="../html/aProposT.php" class="list-group-item">&Agrave; propos</a>
                <a href="../html/conteneursT.php" class="list-group-item">Conteneurs</a>
                <a href="../html/inscriptionT.php" class="list-group-item">Inscription</a>
            </div>

            <h4>Articles Récents</h4>
            <a href="http://www.atelier.net/trends/articles/fret-maritime-gerer-complexite-grace-software_439129" class="list-group-item">
                <h4 class="list-group-item-heading">Fret maritime : gérer la complexité grâce au software</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://www.lepoint.fr/economie/transport-maritime-cma-cgm-veut-racheter-neptune-orient-lines-07-12-2015-1987771_28.php" class="list-group-item">
                <h4 class="list-group-item-heading">Transport maritime : CMA CGM veut racheter Neptune Orient Lines</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://www.francetvinfo.fr/meteo/climat/cop21/cop21-pourquoi-l-accord-ne-suffira-pas-pour-sauver-la-planete_1221853.html" class="list-group-item">
                <h4 class="list-group-item-heading">COP21 : pourquoi l'accord ne suffira pas pour sauver la planète</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://www.liberation.fr/planete/2015/12/10/les-negociateurs-reculent-face-aux-transports-aeriens-et-maritimes_1419759" class="list-group-item">
                <h4 class="list-group-item-heading">Les négociateurs reculent face aux transports aériens et maritimes</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://meretmarine.com/fr/node/136695" class="list-group-item">
                <h4 class="list-group-item-heading">CMA CGM : Le plus gros porte-conteneurs accueilli aux USA</h4>
                <p class="list-group-item-text"></p>
            </a>
        </aside>
    </div>
</section>
<?php include_once '../html/piedPage.php'; ?>

<script src="../java/jquery.js"></script>
<script src="../bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
</body>
</html>