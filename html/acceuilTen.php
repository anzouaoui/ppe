<!DOCTYPE html>
<?php include_once '../html/header.php'; ?>
<section class="main container">
    <div class="miga-de-pan">
        <ol class="breadcrumb">
            <li><a href="../html/acceuilTen.php">Home page</a></li>
            <li><a href="../html/aProposTen.php">About</a></li>
            <li><a href="../html/conteneursTen.php">containers</a></li>
        </ol>
    </div>

    <div class="row">
        <section class="posts col-md-9">
            <div class="miga-de-pan">
                <ol class="breadcrumb">
                    <li class="active">Home</li>
                </ol>
            </div>

            <article class="post clearfix">
                <a href="#" class="thumb pull-left">
                    <img class="img-thumbnail" src="../image/logo.PNG" alt="">
                </a>
                <p class="post-contenido text-justify">
                    In France include :<br>
                    &#8226; 72% of imports and exports <br>
                    &#8226; 15 million passengers each year <br>
                    &#8226; 214 merchant ships <br>
                    &#8226; + 100 million tons of cargo each year <br><br>
                    <iframe class="posts col-md-6" width="520" height="315" src="https://www.youtube.com/embed/-TmEqA7wHQI" frameborder="0" allowfullscreen></iframe><br>
                    &#8594; 90% of goods transported worldwide <br>
                    &#8594; 25,000 billion tons by the SEA <br>
                    7000 billion for RAILS <br>
                    3000 billion for ROADS <br><br>
                    &#8594; Maritime transport is environmentally friendly (7 times less polluting, consumes 7 times less energy than road transport).  <br>
                    &#8594; It is also a sector that creates jobs. At sea and on land, it offers highly qualified and diversified employment: 10 000 6500 seafarers and sedentary. <br>
                    &#8594; The cost of carriage by sea is very competitive include: 
                    0.10€ 1 kg of coffee, less than one cent per liter of petrol.<br>
                    &#8594; It is a key player in the economy. Its large capacity, productivity, environmental performance and high technology marine services 
                    make this sector a promising sector.<br>
                </p>
            </article>
            <!-- plusieurs pages -->
            <!--<nav>
                    <div class="center-block">
                            <ul class="pagination">
                                    <li class="disabled"><a href="#">&laquo;<span class="sr-only">Anterior</span></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">&raquo; <span class="sr-only">Siguiente</span></a></li>
                            </ul>
                    </div
            </nav>-->
        </section>
        <aside class="col-md-3 hidden-xs hidden-sm">
            <h4>Category</h4>
            <div class="list-group">
                <a href="../html/acceuilTen.php" class="list-group-item active">Home page</a>
                <a href="../html/aProposTen.php" class="list-group-item">About</a>
                <a href="../html/conteneursTen.php" class="list-group-item">Containers</a>
            </div>

            <h4>Recent articles</h4>
            <a href="http://www.atelier.net/trends/articles/fret-maritime-gerer-complexite-grace-software_439129" class="list-group-item">
                <h4 class="list-group-item-heading">Fret maritime : gérer la complexité grâce au software</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://www.lepoint.fr/economie/transport-maritime-cma-cgm-veut-racheter-neptune-orient-lines-07-12-2015-1987771_28.php" class="list-group-item">
                <h4 class="list-group-item-heading">Transport maritime : CMA CGM veut racheter Neptune Orient Lines</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://www.francetvinfo.fr/meteo/climat/cop21/cop21-pourquoi-l-accord-ne-suffira-pas-pour-sauver-la-planete_1221853.html" class="list-group-item">
                <h4 class="list-group-item-heading">COP21 : pourquoi l'accord ne suffira pas pour sauver la planète</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://www.liberation.fr/planete/2015/12/10/les-negociateurs-reculent-face-aux-transports-aeriens-et-maritimes_1419759" class="list-group-item">
                <h4 class="list-group-item-heading">Les négociateurs reculent face aux transports aériens et maritimes</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://meretmarine.com/fr/node/136695" class="list-group-item">
                <h4 class="list-group-item-heading">CMA CGM : Le plus gros porte-conteneurs accueilli aux USA</h4>
                <p class="list-group-item-text"></p>
            </a>
        </aside>
    </div>
</section>

<?php include_once '../html/footer.php'; ?>

<script src="../java/jquery.js"></script>
<script src="../bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
</body>
</html>