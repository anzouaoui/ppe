<!DOCTYPE html>
<?php
include '../html/entete.php';
include_once '../php/_gestionBase.inc.php';
?>
<html lang="fr">
    <head>
        <link rel="stylesheet" href="../css/coResT.css">
        <link rel="stylesheet" href="../bootstrap-3.3.4-dist/css/datepicker.css"> 
        <link rel="stylesheet" href="../bootstrap-3.3.4-dist/css/bootstrap.css">
    </head>
    <!-- Début -->

    <div class="container">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="../html/coResT.php" type="button" class="btn btn-primary btn-circle " disabled="disabled">1</a>
                    <p>&Eacute;tape 1</p>
                </div>
                <div class="stepwizard-step">
                    <a href="../html/coResT2.php" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p>&Eacute;tape 2</p>
                </div>
                <div class="stepwizard-step">
                    <a href="../html/coResT3.php" type="button" class="btn btn-default btn-circle active" >3</a>
                    <p>&Eacute;tape 3</p>
                </div>
                <div class="stepwizard-step">
                    <a href="../html/devis.php" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                    <p>&Eacute;tape 4</p>
                </div>
            </div>
        </div>
        <form>
            <div class="col-xs-12">
                <div class="col-md-12">
                    <h3>Récapitulatif</h3>
                    <div class="container">
                        <div class="col-lg-12">
                            <?php
                            $collectionReservation = obtenirReservation();
                            if ($collectionReservation != false):
                                foreach ($collectionReservation as $reservationCourante):
                                    ?>
                                    <p><span>Réserver du:</span><em class="marge_date"><?php echo $reservationCourante["dateDebutReservation"]; ?></em><em id="espace"><span>au: </span></em><em class="marge_date"><?php echo $reservationCourante["dateFinReservation"]; ?></em></p>
                                    <br />
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php
                            $collectionVilleDepart = obtenirVilleMiseDispo();
                            if ($collectionVilleDepart != false):
                                foreach ($collectionVilleDepart as $villeDepartCourante):
                                    ?>    
                                    <p><span>Chargement: </span><em class="marge"><?php echo $villeDepartCourante["nomVille"]; ?></em></p>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php
                            $collectionVilleArrivee = obtenirVilleRendre();
                            if ($collectionVilleArrivee != false):
                                foreach ($collectionVilleArrivee as $villeArriveeCourante):
                                    ?>        
                                    <p><span>Déchargement: </span><em class="marge"><?php echo $villeArriveeCourante["nomVille"]; ?></em></p>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <br />

                            <table class="choix_rangees">
                                <tr>
                                    <th>
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    </th>
                                    <th>
                                        <label>CONTENEUR</label>
                                    </th>
                                    <th>
                                        <label>DIMENSION</label>
                                    </th>
                                    <th>
                                        <label>QUANTITE</label>
                                </tr>
                                <?php
                                $listeReservation = afficherConteneur();
                                if ($listeReservation != false):
                                    foreach ($listeReservation as $reservationCourant):
                                        ?>
                                        <tr class="pas_coche" id="rangeenoi">
                                            <td>
                                                <input type="checkbox" id="cocheri" name="cocheri" onclick="return choix_rangees_cocher('cocher1', 'rangeeno1');" onkeyup="return choix_rangees_cocher('cocher1', 'rangeeno1');">
                                            </td>
                                            <td>
                                                <label for="cocher1"><?php echo $reservationCourant["libelleTypeContainer"]; ?></label>
                                            </td>
                                            <td>
                                                <em>Longueur:</em> <?php echo $reservationCourant["longueurCont"]; ?>mm - <em>Largeur:</em> <?php echo $reservationCourant["largeurCont"]; ?>mm - <em>Hauteur:</em> <?php echo $reservationCourant["hauteurCont"]; ?>mm - <em>Capacite:</em><?php echo $reservationCourant["capaciteDeCharge"]; ?>CU.M
                                            </td>
                                            <td>
                                                <?php echo $reservationCourant["qteReserver"]; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </table>
                            <br />
                            <br />
                            <div class="col-lg-9">
                                <button class="btn btn-success btn-lg pull-right" type="submit">Supprimer</button>
                            </div>
                            <div class="col-lg-1">
                                <a href="../html/coResT2.php">
                                    <button class="btn btn-success btn-lg pull-right" type="button">Retour</button>
                                </a>
                            </div>
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <div class="col-lg-2 col-lg-offset-10">
                                <div>
                                    <a href="../php/devis2.traitement.php">
                                        <button class="btn btn-success btn-lg pull-right" type="button">Générer Devis</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <?php include_once '../html/piedPage.php'; ?>

                <script src="../java/jquery.js"></script>
                <script src="../bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
                <script src="../java/coResT.js"></script>
                <script src="../java/coResT2.js"></script>
                <script src="../jquery/main.js"></script>
                <script src="../bootstrap-3.3.4-dist/js/bootstrap.js"></script>
                <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.js"></script>-->
                </body>
                </html>
                <!--
                http://bootsnipp.com/snippets/featured/form-wizard-and-validation
                http://bootsnipp.com/snippets/featured/float-label-pattern-forms
                http://bootsnipp.com/snippets/3kDmD
                -->

