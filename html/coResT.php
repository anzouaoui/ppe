<!DOCTYPE html>
<?php
include '../html/entete.php';
include_once '../php/_gestionBase.inc.php';
?>
<html lang="fr">
    <head>
        <link rel="stylesheet" href="../css/coResT.css">
        <link rel="stylesheet" href="../bootstrap-3.3.4-dist/css/datepicker.css"> 
        <link rel="stylesheet" href="../bootstrap-3.3.4-dist/css/bootstrap.css">
    </head>
    <!-- Début -->

    <div class="container">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="../html/coResT.php" type="button" class="btn btn-primary btn-circle active">1</a>
                    <p>&Eacute;tape 1</p>
                </div>
                <div class="stepwizard-step">
                    <a href="../html/coResT2.php" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p>&Eacute;tape 2</p>
                </div>
                <div class="stepwizard-step">
                    <a href="../html/coResT3.php" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p>&Eacute;tape 3</p>
                </div>
                <div class="stepwizard-step">
                    <a href="../html/devis.php" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                    <p>&Eacute;tape 4</p>
                </div>
            </div>
        </div>
        <form role="form" role="form" method="post" action="../php/reservation.traitement.php">
            <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12 ">
                <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12 ">
                    <h3>Réservation</h3>
                    <div class="well-lg">
                        <div class="container  ">
                            <div class="col-md-6 col-lg-6 col-sm-6 col-xd-6">
                                <label class="control-label col-sm-2" for="dateDebutReservation">Réservé du:</label>
                                <div class="form-group">
                                    <div class='input-group date'>
                                        <input type='date' class="form-control" id='datetimepicker1' name="dateDebutReservation" required />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="control-label col-sm-2" for="dateFinReservation">au:</label>
                                <div class="form-group">
                                    <div class='input-group date'>
                                        <input type='date' class="form-control"  name="dateFinReservation" id='datetimepicker2' required/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="volumeEstime">Volume estimé:</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" id="adresse" placeholder="1 " required name="volumeEstime">
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="codeVilleMiseDispo">Port de chargement:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="codeVilleMiseDispo" >
                                    <?php $collectionVilleMiseDispo = obtenirVille(); ?>

                                    <?php foreach ($collectionVilleMiseDispo as $ville): ?>

                                        <option   value="<?php echo $ville["codeVille"]; ?>"><?php echo $ville["nomVille"]; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group" >
                            <label class="control-label col-sm-2" for="codeVilleRendre">Port de déchargement:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="codeVilleRendre">
                                    <?php $collectionVilleRendre = obtenirVille(); ?>

                                    <?php foreach ($collectionVilleRendre as $ville): ?>
                                        <option  value="<?php echo $ville["codeVille"]; ?>"><?php echo $ville["nomVille"]; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <br />
                        <br />
                        
                        

                        <div>
                            <button class="btn btn-primary nextBtn btn-lg pull-right" type="submit" >Suite</button>
                        </div>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
        </form>

    </div>
    <?php include_once '../html/piedPage.php'; ?>

    <script src="../jquery/main.js"></script>
    <script src="../bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
    <script src="../java/coResT.js"></script>
    <script src="../java/coResT2.js"></script>
    <script src="../java/recapitulatif.js"></script>
    <script src="../java/date.js"></script>
    <script src="../bootstrap-3.3.4-dist/js/bootstrap-datepicker.js"></script>
    <script src="../bootstrap-3.3.4-dist/js/bootstrap.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.js"></script>-->
</body>
</html>
<!--
http://bootsnipp.com/snippets/featured/form-wizard-and-validation
http://bootsnipp.com/snippets/featured/float-label-pattern-forms
http://bootsnipp.com/snippets/3kDmD
-->